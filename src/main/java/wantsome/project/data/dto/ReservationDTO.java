package wantsome.project.data.dto;

import java.sql.Date;
import java.util.Objects;

public class ReservationDTO {
    private int id;
    private ClientDTO client;
    private RoomDTO room;
    private Date startDate;
    private Date endDate;


    public ReservationDTO(ClientDTO client, RoomDTO room, Date startDate, Date endDate) {
        this.client = client;
        this.room = room;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ReservationDTO(int clientId, int roomId, Date startDate, Date endDate) {
        this(new ClientDTO(clientId), new RoomDTO(roomId), startDate, endDate);
    }

    public ReservationDTO(int id, ClientDTO client, RoomDTO room, Date startDate, Date endDate) {
        this(client, room, startDate, endDate);
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public ClientDTO getClient() {
        return client;
    }

    public RoomDTO getRoom() {
        return room;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationDTO that = (ReservationDTO) o;
        return id == that.id &&
                Objects.equals(client, that.client) &&
                Objects.equals(room, that.room) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, room, startDate, endDate);
    }

    @Override
    public String toString() {
        return "ReservationDTO{" +
                "id=" + id +
                ", client=" + client +
                ", room=" + room +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
