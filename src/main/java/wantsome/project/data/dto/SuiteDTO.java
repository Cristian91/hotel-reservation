package wantsome.project.data.dto;

import java.util.Objects;

public class SuiteDTO {
    private int id;
    private SuiteType type;
    private int size;
    private int price;

    public SuiteDTO(int id, SuiteType type, int size, int price) {
        this(type, size, price);
        this.id = id;
    }

    public SuiteDTO(int id) {
        this.id = id;
    }

    public SuiteDTO(SuiteType type, int size, int price) {
        this.type = type;
        this.size = size;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public SuiteType getType() {
        return type;
    }

    public int getSize() {
        return size;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuiteDTO suiteDTO = (SuiteDTO) o;
        return id == suiteDTO.id &&
                size == suiteDTO.size &&
                price == suiteDTO.price &&
                type == suiteDTO.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, size, price);
    }

    @Override
    public String toString() {
        return "SuiteDTO{" +
                "id=" + id +
                ", type=" + type +
                ", size=" + size +
                ", price=" + price +
                '}';
    }
}
