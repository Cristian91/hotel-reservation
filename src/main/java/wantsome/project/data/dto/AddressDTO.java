package wantsome.project.data.dto;

import java.util.Objects;

public class AddressDTO {
    private int id;
    private String streetNumber;
    private String zipCode;
    private String city;
    private String country;

    public AddressDTO(String streetNumber, String zipCode, String city, String country) {
        this.streetNumber = streetNumber;
        this.zipCode = zipCode;
        this.city = city;
        this.country = country;
    }

    public AddressDTO(int id, String streetNumber, String zipCode, String city, String country) {
        this(streetNumber, zipCode, city, country);
        this.id = id;
    }

    public AddressDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressDTO that = (AddressDTO) o;
        return id == that.id &&
                Objects.equals(streetNumber, that.streetNumber) &&
                Objects.equals(zipCode, that.zipCode) &&
                Objects.equals(city, that.city) &&
                Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, streetNumber, zipCode, city, country);
    }

    @Override
    public String toString() {
        return "AddressDTO{" +
                "id=" + id +
                ", streetNumber='" + streetNumber + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
