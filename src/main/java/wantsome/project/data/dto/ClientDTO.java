package wantsome.project.data.dto;

import java.util.Objects;

public class ClientDTO {
    private int id;
    private String firstName;
    private String lastName;
    private long cnp;
    private String email;
    private AddressDTO address;

    public ClientDTO(String firstName, String lastName, long cnp, String email, AddressDTO address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.email = email;
        this.address = address;
    }

    public ClientDTO(int id, String firstName, String lastName, long cnp, String email, AddressDTO address) {
        this(firstName, lastName, cnp, email, address);
        this.id = id;
    }

    public ClientDTO(String firstName, String lastName, long cnp, String email, int addressId) {
        this(firstName, lastName, cnp, email, new AddressDTO(addressId));
    }

    public ClientDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getCnp() {
        return cnp;
    }

    public String getEmail() {
        return email;
    }

    public AddressDTO getAddress() {
        return address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientDTO clientDTO = (ClientDTO) o;
        return id == clientDTO.id &&
                cnp == clientDTO.cnp &&
                Objects.equals(firstName, clientDTO.firstName) &&
                Objects.equals(lastName, clientDTO.lastName) &&
                Objects.equals(email, clientDTO.email) &&
                Objects.equals(address, clientDTO.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, cnp, email, address);
    }

    @Override
    public String toString() {
        return "ClientDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cnp=" + cnp +
                ", email='" + email + '\'' +
                ", address=" + address +
                '}';
    }
}
