package wantsome.project.data.dto;

public enum SuiteType {
    SINGLE, DOUBLE, TRIPLE, PENTHOUSE
}
