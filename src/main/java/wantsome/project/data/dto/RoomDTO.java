package wantsome.project.data.dto;

import java.util.Objects;

public class RoomDTO {
    private int id;
    private int roomNumber;
    private SuiteDTO suiteDTO;
    private String description;

    public RoomDTO(int id, int roomNumber, SuiteDTO suiteDTO, String description) {
        this(roomNumber, suiteDTO, description);
        this.id = id;
    }

    public RoomDTO(int roomNumber, SuiteDTO suiteDTO, String description) {
        this.roomNumber = roomNumber;
        this.suiteDTO = suiteDTO;
        this.description = description;
    }

    public RoomDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public SuiteDTO getSuiteDTO() {
        return suiteDTO;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomDTO roomDTO = (RoomDTO) o;
        return id == roomDTO.id &&
                roomNumber == roomDTO.roomNumber &&
                Objects.equals(suiteDTO, roomDTO.suiteDTO) &&
                Objects.equals(description, roomDTO.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomNumber, suiteDTO, description);
    }

    @Override
    public String toString() {
        return "RoomDTO{" +
                "id=" + id +
                ", roomNumber=" + roomNumber +
                ", suiteDTO=" + suiteDTO +
                ", description='" + description + '\'' +
                '}';
    }
}
