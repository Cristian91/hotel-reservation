package wantsome.project.data.dao;

import wantsome.project.data.dto.SuiteDTO;
import wantsome.project.data.dto.SuiteType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static wantsome.project.data.dao.UtilDAO.ID_COLUMN;
import static wantsome.project.data.dao.UtilDAO.getConnection;

public class SuiteDAO {
    public static final String SUITE_TABLE = "Suite";
    public static final String TYPE_COLUMN = "Type";
    public static final String SIZE_COLUMN = "Size";
    public static final String PRICE_PER_NIGHT_COLUMN = "PricePerNight";
    public static final String CURRENCY = "Euro";

    public static final String CREATE_SUITE_TABLE = "CREATE TABLE IF NOT EXISTS " + SUITE_TABLE + '(' +
            ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            TYPE_COLUMN + " VARCHAR(255) CHECK(" + TYPE_COLUMN + " IN ('" +
            SuiteType.SINGLE + "', '" + SuiteType.DOUBLE + "', '" +
            SuiteType.TRIPLE + "', '" + SuiteType.PENTHOUSE + "')) NOT NULL UNIQUE, " +
            SIZE_COLUMN + " INTEGER NOT NULL UNIQUE, " +
            PRICE_PER_NIGHT_COLUMN + " INTEGER NOT NULL);";

    public static void insert(SuiteDTO suiteDTO) {
        String saveSuiteStatement = "INSERT INTO " + SUITE_TABLE +
                "(" + TYPE_COLUMN + ", " +
                SIZE_COLUMN + ", " +
                PRICE_PER_NIGHT_COLUMN + ")" +
                " VALUES(?,?,?)";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(saveSuiteStatement)) {

            ps.setString(1, suiteDTO.getType().toString());
            ps.setInt(2, suiteDTO.getSize());
            ps.setInt(3, suiteDTO.getPrice());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Could not add Suite.");
        }
    }

    public static void update(int id, int price) {
        String updateSQL = "UPDATE " + SUITE_TABLE +
                " SET " + PRICE_PER_NIGHT_COLUMN + " =? " +
                " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(updateSQL)) {

            ps.setInt(1, price);
            ps.setInt(2, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not update Suite.");
        }
    }

    public static SuiteDTO findByType(String suiteType) {
        String selectQuery = "SELECT * FROM " + SUITE_TABLE +
                " WHERE " + TYPE_COLUMN + " =?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(selectQuery)) {

            ps.setString(1, suiteType);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return mapSuiteDTO(rs);
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException("Could not find Suite for type: " + suiteType);
        }
        return null;
    }

    public static List<SuiteDTO> findAll() {
        String selectQuery = "SELECT * FROM " + SUITE_TABLE;
        return UtilDAO.findAll(selectQuery, SuiteDAO::mapSuiteDTO);
    }

    private static SuiteDTO mapSuiteDTO(ResultSet rs) {
        try {
            return new SuiteDTO(
                    rs.getInt(ID_COLUMN),
                    SuiteType.valueOf(rs.getString(TYPE_COLUMN)),
                    rs.getInt(SIZE_COLUMN),
                    rs.getInt(PRICE_PER_NIGHT_COLUMN)
            );

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
