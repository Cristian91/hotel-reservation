package wantsome.project.data.dao;

import wantsome.project.data.dto.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static wantsome.project.data.dao.AddressDAO.*;
import static wantsome.project.data.dao.ClientDAO.*;
import static wantsome.project.data.dao.RoomDAO.*;
import static wantsome.project.data.dao.SuiteDAO.*;
import static wantsome.project.data.dao.SuiteDAO.PRICE_PER_NIGHT_COLUMN;
import static wantsome.project.data.dao.UtilDAO.ID_COLUMN;
import static wantsome.project.data.dao.UtilDAO.getConnection;

public class ReservationDAO {
    public static final String RESERVATION_TABLE = "Reservation";
    public static final String CLIENT_ID_COLUMN = "ClientId";
    public static final String ROOM_ID_COLUMN = "RoomId";
    public static final String START_DATE_COLUMN = "StartDate";
    public static final String END_DATE_COLUMN = "EndDate";
    public static final String CREATE_RESERVATION_TABLE =
            "CREATE TABLE IF NOT EXISTS " + RESERVATION_TABLE + '(' +
                    ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    CLIENT_ID_COLUMN + " VARCHAR(255) REFERENCES " +
                    CLIENT_TABLE + '(' + ID_COLUMN + "), " +
                    ROOM_ID_COLUMN + " VARCHAR(255) REFERENCES " +
                    ROOM_TABLE + '(' + ID_COLUMN + "), " +
                    START_DATE_COLUMN + " DATE, " +
                    END_DATE_COLUMN + " DATE);";

    private static final String SELECT_ALL_QUERY =
            "SELECT * FROM " + RESERVATION_TABLE + " AS RES " +
                    " JOIN " + CLIENT_TABLE + " AS CT ON RES." + CLIENT_ID_COLUMN + " = CT." + ID_COLUMN +
                    " JOIN " + ADDRESS_TABLE + " AS AT ON CT." + ADDRESS_ID_COLUMN + " = AT." + ID_COLUMN +
                    " JOIN " + ROOM_TABLE + " AS RT ON RES." + ROOM_ID_COLUMN + " = RT." + ID_COLUMN +
                    " JOIN " + SUITE_TABLE + " AS ST ON RT." + SUITE_ID_COLUMN + " = ST." + ID_COLUMN;

    public static void insert(ReservationDTO reservationDTO) {
        String saveReservationStatement = "INSERT INTO " + RESERVATION_TABLE +
                "(" + CLIENT_ID_COLUMN + ", " +
                ROOM_ID_COLUMN + ", " +
                START_DATE_COLUMN + ", " +
                END_DATE_COLUMN + ") " +
                " VALUES(?,?,?,?)";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(saveReservationStatement)) {

            ps.setInt(1, reservationDTO.getClient().getId());
            ps.setInt(2, reservationDTO.getRoom().getId());
            ps.setDate(3, reservationDTO.getStartDate());
            ps.setDate(4, reservationDTO.getEndDate());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Could not add Reservation.");
        }
    }

    public static void delete(int id) {
        String deleteSQL = "DELETE FROM " + RESERVATION_TABLE + " WHERE ID = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(deleteSQL)) {

            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not delete Reservation.");
        }
    }

    public static List<ReservationDTO> findAll() {
        String selectSQL = SELECT_ALL_QUERY + " ORDER BY RES." + ID_COLUMN + " DESC;";
        return UtilDAO.findAll(selectSQL, ReservationDAO::mapReservationDTO);
    }

    public static ReservationDTO getReservationById(int idReservation) {
        String reservationData = SELECT_ALL_QUERY + " WHERE RES." + ID_COLUMN + "=? ;";

        try (Connection conn = getConnection();
             PreparedStatement pst = conn.prepareStatement(reservationData)) {

            pst.setInt(1, idReservation);

            try (ResultSet rs = pst.executeQuery()) {

                while (rs.next()) {
                    return mapReservationDTO(rs);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find Reservation for id: " + idReservation);
        }
        return new ReservationDTO(-1, -1, Date.valueOf("1900-10-10"), Date.valueOf("1900-10-10"));
    }

    public static List<ReservationDTO> getReservationByClient(String customerFirstName, String customerLastName, long cnp) {

        String reservationData = SELECT_ALL_QUERY +
                " WHERE " + FIRST_NAME_COLUMN + "=? AND " + LAST_NAME_COLUMN + "=? " +
                "AND " + CNP_COLUMN + "=?;";

        List<ReservationDTO> clientReservations = new ArrayList<>();

        try (Connection conn = getConnection();
             PreparedStatement pst = conn.prepareStatement(reservationData)) {

            pst.setString(1, customerFirstName.toUpperCase());
            pst.setString(2, customerLastName.toUpperCase());
            pst.setLong(3, cnp);

            try (ResultSet rs = pst.executeQuery()) {

                while (rs.next()) {
                    clientReservations.add(mapReservationDTO(rs));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find Reservation for Client: " +
                    customerFirstName + " " + customerLastName + " with cnp: " + cnp);
        }
        if (clientReservations.isEmpty()) {
            throw new RuntimeException("Could not find the specified Client.");
        }
        return clientReservations;
    }

    public static int getPriceToPay(int idReservation) {
        String infoSQL = SELECT_ALL_QUERY + " WHERE RES." + ID_COLUMN + " =? ;";

        Date startDate = new Date(System.currentTimeMillis());
        Date endDate = new Date(System.currentTimeMillis());
        int pricePerNight = 0;
        int totalPriceToPay = 0;

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(infoSQL)) {

            ps.setInt(1, idReservation);

            try (ResultSet rs = ps.executeQuery()) {

                while (rs.next()) {
                    startDate = (rs.getDate(START_DATE_COLUMN));
                    endDate = (rs.getDate(END_DATE_COLUMN));
                    pricePerNight = (rs.getInt(PRICE_PER_NIGHT_COLUMN));
                }
                //formula to determine days spent (1 day = 86400000 ms)
                int numberOfDaysSpent = (int) ((Math.abs(endDate.getTime() - startDate.getTime()) / 86400000) + 1);
                totalPriceToPay = pricePerNight * numberOfDaysSpent;
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not get Price.");
        }
        return totalPriceToPay;
    }

    public static List<RoomDTO> getAvailableRoomsByDate(Date startDate, Date endDate) {
        String reservationData =
                "SELECT * " +
                        " FROM " + ROOM_TABLE + " AS R" +
                        " JOIN " + SUITE_TABLE + " AS S ON R." + SUITE_ID_COLUMN + " = S." + ID_COLUMN +
                        " WHERE R." + ID_COLUMN + " NOT IN " +
                        " (SELECT " + ROOM_ID_COLUMN +
                        " FROM " + RESERVATION_TABLE +
                        " WHERE ( ? BETWEEN " + START_DATE_COLUMN + " AND " + END_DATE_COLUMN + " ) OR " +
                        " (? BETWEEN " + START_DATE_COLUMN + " AND " + END_DATE_COLUMN + " ) " +
                        " OR (? < " + START_DATE_COLUMN + " AND ? > " + END_DATE_COLUMN + "));";

        List<RoomDTO> availableRooms = new ArrayList<>();

        try (Connection conn = getConnection();
             PreparedStatement pst = conn.prepareStatement(reservationData)) {

            pst.setDate(1, startDate);
            pst.setDate(2, endDate);
            pst.setDate(3, startDate);
            pst.setDate(4, endDate);


            try (ResultSet rs = pst.executeQuery()) {

                while (rs.next()) {
                    SuiteDTO suite =
                            new SuiteDTO(
                                    SuiteType.valueOf(rs.getString(TYPE_COLUMN)),
                                    rs.getInt(SIZE_COLUMN),
                                    rs.getInt(PRICE_PER_NIGHT_COLUMN)
                            );

                    availableRooms.add(new RoomDTO(
                            rs.getInt(ID_COLUMN),
                            rs.getInt(ROOM_NUMBER_COLUMN),
                            suite,
                            rs.getString(DESCRIPTION_COLUMN))
                    );
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find available Rooms.");
        }
        if (availableRooms.isEmpty()) {
            throw new RuntimeException("No available Rooms in period: " + startDate + " and " + endDate);
        }
        return availableRooms;
    }

    private static ReservationDTO mapReservationDTO(ResultSet rs) {
        try {
            AddressDTO addressClient =
                    new AddressDTO(
                            rs.getString(STREET_NUMBER_COLUMN),
                            rs.getString(ZIP_CODE_COLUMN),
                            rs.getString(CITY_COLUMN),
                            rs.getString(COUNTRY_COLUMN)
                    );

            ClientDTO client =
                    new ClientDTO(
                            rs.getInt(ID_COLUMN),
                            rs.getString(FIRST_NAME_COLUMN),
                            rs.getString(LAST_NAME_COLUMN),
                            rs.getLong(CNP_COLUMN),
                            rs.getString(EMAIL_COLUMN),
                            addressClient
                    );

            SuiteDTO suite =
                    new SuiteDTO(
                            SuiteType.valueOf(rs.getString(TYPE_COLUMN)),
                            rs.getInt(SIZE_COLUMN),
                            rs.getInt(PRICE_PER_NIGHT_COLUMN)
                    );

            RoomDTO roomClient =
                    new RoomDTO(
                            rs.getInt(ID_COLUMN),
                            rs.getInt(ROOM_NUMBER_COLUMN),
                            suite,
                            rs.getString(DESCRIPTION_COLUMN)
                    );

            return new ReservationDTO(
                    rs.getInt(ID_COLUMN),
                    client,
                    roomClient,
                    rs.getDate(START_DATE_COLUMN),
                    rs.getDate(END_DATE_COLUMN)
            );
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
