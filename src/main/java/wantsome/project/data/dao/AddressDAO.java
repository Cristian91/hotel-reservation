package wantsome.project.data.dao;

import wantsome.project.data.dto.AddressDTO;

import java.sql.*;
import java.util.List;

import static wantsome.project.data.dao.UtilDAO.ID_COLUMN;
import static wantsome.project.data.dao.UtilDAO.getConnection;

public class AddressDAO {

    public static final String ADDRESS_TABLE = "Address";
    public static final String STREET_NUMBER_COLUMN = "StreetNumber";
    public static final String ZIP_CODE_COLUMN = "ZipCode";
    public static final String CITY_COLUMN = "City";
    public static final String COUNTRY_COLUMN = "Country";

    public static final String CREATE_ADDRESS_TABLE = "CREATE TABLE IF NOT EXISTS " + ADDRESS_TABLE + " ( " +
            ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            STREET_NUMBER_COLUMN + " varchar(255) NOT NULL, " +
            ZIP_CODE_COLUMN + " varchar(255) NOT NULL, " +
            CITY_COLUMN + " varchar(255) NOT NULL, " +
            COUNTRY_COLUMN + " varchar(255) NOT NULL); ";

    public static void insert(AddressDTO addressDTO) {
        String saveAddressStatement = "INSERT INTO " + ADDRESS_TABLE +
                " (" + STREET_NUMBER_COLUMN + ", " +
                ZIP_CODE_COLUMN + ", " +
                CITY_COLUMN + ", " +
                COUNTRY_COLUMN + ")" +
                " VALUES(?,?,?,?);";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(saveAddressStatement)) {

            ps.setString(1, addressDTO.getStreetNumber());
            ps.setString(2, addressDTO.getZipCode());
            ps.setString(3, addressDTO.getCity());
            ps.setString(4, addressDTO.getCountry());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Could not add Address.");
        }
    }

    public static void update(AddressDTO addressDTO) {
        String updateSQL = " UPDATE " + ADDRESS_TABLE +
                " SET " + STREET_NUMBER_COLUMN + " = ? , " +
                ZIP_CODE_COLUMN + " = ? , " +
                CITY_COLUMN + " = ? , " +
                COUNTRY_COLUMN + " = ? " +
                " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(updateSQL)) {

            ps.setString(1, addressDTO.getStreetNumber());
            ps.setString(2, addressDTO.getZipCode());
            ps.setString(3, addressDTO.getCity());
            ps.setString(4, addressDTO.getCountry());
            ps.setInt(5, addressDTO.getId());
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not update Address.");
        }
    }

    public static void delete(int id) {
        String deleteSQL = "DELETE FROM " + ADDRESS_TABLE + " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(deleteSQL)) {

            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not delete Address.");
        }
    }

    public static int getLastIdInserted() {
        String getLastIdInserted = "SELECT " + ID_COLUMN + " FROM " + ADDRESS_TABLE +
                " WHERE " + ID_COLUMN + " = (select MAX(" + ID_COLUMN + ") from " + ADDRESS_TABLE + ");";
        int lastIdInserted = -1;
        try (Connection conn = getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(getLastIdInserted)) {

            lastIdInserted = rs.getInt(ID_COLUMN);

        } catch (SQLException throwables) {
            throw new RuntimeException("Could not get last id inserted from Address.");
        }
        return lastIdInserted;
    }

    public static List<AddressDTO> findAll() {
        String selectQuery = "SELECT * FROM " + ADDRESS_TABLE +
                " ORDER BY " + ID_COLUMN + " DESC;";
        return UtilDAO.findAll(selectQuery, AddressDAO::mapAddressDTO);
    }

    private static AddressDTO mapAddressDTO(ResultSet rs) {
        try {
            return new AddressDTO(
                    rs.getInt(ID_COLUMN),
                    rs.getString(STREET_NUMBER_COLUMN),
                    rs.getString(ZIP_CODE_COLUMN),
                    rs.getString(CITY_COLUMN),
                    rs.getString(COUNTRY_COLUMN)
            );
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
