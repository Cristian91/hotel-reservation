package wantsome.project.data.dao;

import org.sqlite.SQLiteConfig;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static wantsome.project.data.dao.SuiteDAO.CURRENCY;

public final class UtilDAO {
    public static final String ID_COLUMN = "ID";

    private UtilDAO() {
    }

    public static Connection getConnection() throws SQLException {
        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true);
        config.setDateStringFormat("yyyy-MM-dd HH:mm:ss");

        return DriverManager.getConnection("jdbc:sqlite:HotelReservation.db", config.toProperties());
    }

    public static String getCurrency() {
        return CURRENCY;
    }

    public static <T> List<T> findAll(String findAllQuery, Function<ResultSet, T> dtoMapFunction) {
        List<T> dtoList = new ArrayList<>();

        try (Connection conn = getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(findAllQuery)) {

            while (rs.next()) {
                dtoList.add(dtoMapFunction.apply(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return dtoList;
    }
}
