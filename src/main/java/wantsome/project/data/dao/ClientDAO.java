package wantsome.project.data.dao;

import wantsome.project.data.dto.AddressDTO;
import wantsome.project.data.dto.ClientDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static wantsome.project.data.dao.AddressDAO.*;
import static wantsome.project.data.dao.UtilDAO.ID_COLUMN;
import static wantsome.project.data.dao.UtilDAO.getConnection;

public class ClientDAO {
    public static final String CLIENT_TABLE = "Client";
    public static final String FIRST_NAME_COLUMN = "FirstName";
    public static final String LAST_NAME_COLUMN = "LastName";
    public static final String CNP_COLUMN = "CNP";
    public static final String EMAIL_COLUMN = "Email";
    public static final String ADDRESS_ID_COLUMN = "AddressId";

    public static final String CREATE_CLIENT_TABLE = " CREATE TABLE IF NOT EXISTS " + CLIENT_TABLE + " ( " +
            ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            FIRST_NAME_COLUMN + " VARCHAR(255) NOT NULL, " +
            LAST_NAME_COLUMN + " VARCHAR(255) NOT NULL, " +
            CNP_COLUMN + " BIGINT NOT NULL UNIQUE, " +
            EMAIL_COLUMN + " VARCHAR(255) NOT NULL UNIQUE, " +
            ADDRESS_ID_COLUMN + " INTEGER NOT NULL REFERENCES " + ADDRESS_TABLE + '(' + ID_COLUMN + "), " +
            " CONSTRAINT UC_CLIENT UNIQUE (" +
            FIRST_NAME_COLUMN + ',' + LAST_NAME_COLUMN + ',' + CNP_COLUMN + "));";


    public static void insert(ClientDTO clientDTO) {
        String saveClientStatement = "INSERT INTO " + CLIENT_TABLE +
                "(" + FIRST_NAME_COLUMN + ", " +
                LAST_NAME_COLUMN + ", " +
                CNP_COLUMN + ", " +
                EMAIL_COLUMN + ", " +
                ADDRESS_ID_COLUMN + ")" +
                " VALUES(?,?,?,?,?)";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(saveClientStatement)) {

            ps.setString(1, clientDTO.getFirstName().toUpperCase());
            ps.setString(2, clientDTO.getLastName().toUpperCase());
            ps.setLong(3, clientDTO.getCnp());
            ps.setString(4, clientDTO.getEmail());
            ps.setInt(5, clientDTO.getAddress().getId());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Could not add the specified Client, he already exists in the Clients table.");
        }
    }

    public static void update(ClientDTO clientDTO) {
        String updateSQL = "UPDATE " + CLIENT_TABLE +
                " SET " + FIRST_NAME_COLUMN + " = ? , " +
                LAST_NAME_COLUMN + " = ? , " +
                CNP_COLUMN + " = ? , " +
                EMAIL_COLUMN + " = ? " +
                " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(updateSQL)) {

            ps.setString(1, clientDTO.getFirstName());
            ps.setString(2, clientDTO.getLastName());
            ps.setLong(3, clientDTO.getCnp());
            ps.setString(4, clientDTO.getEmail());
            ps.setInt(5, clientDTO.getId());
            ps.execute();

        } catch (SQLException throwables) {
            throw new RuntimeException("Could not update Client.");
        }
    }

    public static void delete(int id) {
        String deleteSQL = "DELETE FROM " + CLIENT_TABLE + " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(deleteSQL)) {

            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not delete Client.");
        }
    }

    public static List<ClientDTO> findAll() {
        String selectQuery = "SELECT * FROM " + CLIENT_TABLE + " AS CT" +
                " JOIN " + ADDRESS_TABLE + " AS AT ON CT." + ADDRESS_ID_COLUMN + " = AT." + ID_COLUMN +
                " ORDER BY CT." + ID_COLUMN + " DESC;";
        return UtilDAO.findAll(selectQuery, ClientDAO::mapClientDTO);
    }

    public static Optional<ClientDTO> findClientById(int id) {
        String selectSQL = "SELECT * FROM " + CLIENT_TABLE + " AS CT" +
                " JOIN " + ADDRESS_TABLE + " AS AT ON CT." + ADDRESS_ID_COLUMN + " = AT." + ID_COLUMN +
                " WHERE CT." + ID_COLUMN + "=?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(selectSQL)) {

            ps.setInt(1, id);
            ps.execute();

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    AddressDTO addressClient =
                            new AddressDTO(
                                    rs.getString(STREET_NUMBER_COLUMN),
                                    rs.getString(ZIP_CODE_COLUMN),
                                    rs.getString(CITY_COLUMN),
                                    rs.getString(COUNTRY_COLUMN));

                    return Optional.of(
                            new ClientDTO(
                                    rs.getInt(ID_COLUMN),
                                    rs.getString(FIRST_NAME_COLUMN),
                                    rs.getString(LAST_NAME_COLUMN),
                                    rs.getLong(CNP_COLUMN),
                                    rs.getString(EMAIL_COLUMN),
                                    addressClient));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find Client by id: " + id);
        }
        return Optional.empty();
    }

    public static int findAddressIdByClientId(int idClient) {
        String sql = "SELECT * FROM " + CLIENT_TABLE +
                " WHERE " + ID_COLUMN + " = ?;";
        int addressId = -1;

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, idClient);

            try (ResultSet rs = ps.executeQuery()) {
                addressId = rs.getInt(ADDRESS_ID_COLUMN);
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find Address for clientId: " + idClient);
        }

        return addressId;
    }

    public static List<ClientDTO> getCustomerInfoByNameAndCNP(String customerFirstName, String customerLastName, long customerCNP) {
        String clientData = "SELECT * FROM " + CLIENT_TABLE +
                " JOIN " + ADDRESS_TABLE + " ON " +
                CLIENT_TABLE + '.' + ADDRESS_ID_COLUMN + '=' +
                ADDRESS_TABLE + '.' + ID_COLUMN +
                " WHERE " + FIRST_NAME_COLUMN + "=? AND " +
                LAST_NAME_COLUMN + "=? AND " + CNP_COLUMN + "=? ;";

        List<ClientDTO> clientInformation = new ArrayList<>();

        try (Connection conn = getConnection();
             PreparedStatement pst = conn.prepareStatement(clientData)) {

            pst.setString(1, customerFirstName.toUpperCase());
            pst.setString(2, customerLastName.toUpperCase());
            pst.setLong(3, customerCNP);

            try (ResultSet rs = pst.executeQuery()) {

                while (rs.next()) {
                    AddressDTO addressClient =
                            new AddressDTO(
                                    rs.getString(STREET_NUMBER_COLUMN),
                                    rs.getString(ZIP_CODE_COLUMN),
                                    rs.getString(CITY_COLUMN),
                                    rs.getString(COUNTRY_COLUMN));

                    clientInformation.add(
                            new ClientDTO(
                                    rs.getInt(ID_COLUMN),
                                    rs.getString(FIRST_NAME_COLUMN),
                                    rs.getString(LAST_NAME_COLUMN),
                                    rs.getLong(CNP_COLUMN),
                                    rs.getString(EMAIL_COLUMN),
                                    addressClient));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not retrieve Client info.");
        }
        if (clientInformation.isEmpty()) {
            throw new RuntimeException("The specified Client does not exist in the Clients table.");
        }
        return clientInformation;
    }

    public static int getLastIdInserted() {
        String getLastIdInserted = "SELECT " + ID_COLUMN + " FROM " + CLIENT_TABLE +
                " WHERE " + ID_COLUMN + " = (select MAX(" + ID_COLUMN + ") from " + CLIENT_TABLE + ");";
        int lastIdInserted = -1;
        try (Connection conn = getConnection();
             Statement st = conn.createStatement();
             ResultSet rs = st.executeQuery(getLastIdInserted)) {

            lastIdInserted = rs.getInt(ID_COLUMN);

        } catch (SQLException e) {
            throw new RuntimeException("No id's available in Clients Table.");
        }
        return lastIdInserted;
    }

    public static ClientDTO mapClientDTO(ResultSet rs) {
        try {
            AddressDTO addressClient =
                    new AddressDTO(
                            rs.getString(STREET_NUMBER_COLUMN),
                            rs.getString(ZIP_CODE_COLUMN),
                            rs.getString(CITY_COLUMN),
                            rs.getString(COUNTRY_COLUMN)
                    );

            return new ClientDTO(
                    rs.getInt(ID_COLUMN),
                    rs.getString(FIRST_NAME_COLUMN),
                    rs.getString(LAST_NAME_COLUMN),
                    rs.getLong(CNP_COLUMN),
                    rs.getString(EMAIL_COLUMN),
                    addressClient
            );
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
