package wantsome.project.data.dao;

import wantsome.project.data.dto.*;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static wantsome.project.data.dao.SuiteDAO.*;
import static wantsome.project.data.dao.UtilDAO.ID_COLUMN;
import static wantsome.project.data.dao.UtilDAO.getConnection;

public class RoomDAO {
    public static final String ROOM_TABLE = "Room";
    public static final String ROOM_NUMBER_COLUMN = "RoomNumber";
    public static final String SUITE_ID_COLUMN = "SuiteID";
    public static final String DESCRIPTION_COLUMN = "Description";

    public static final String CREATE_ROOM_TABLE = "CREATE TABLE IF NOT EXISTS " + ROOM_TABLE + '(' +
            ID_COLUMN + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            ROOM_NUMBER_COLUMN + " INTEGER NOT NULL UNIQUE, " +
            SUITE_ID_COLUMN + " INTEGER REFERENCES " + SUITE_TABLE + '(' + ID_COLUMN + "), " +
            DESCRIPTION_COLUMN + " VARCHAR(255));";

    public static void insert(RoomDTO roomDTO) {
        String saveRoomStatement = "INSERT INTO " + ROOM_TABLE +
                "(" + ROOM_NUMBER_COLUMN + ", " +
                SUITE_ID_COLUMN + ", " +
                DESCRIPTION_COLUMN + ')' +
                " VALUES(?,?,?)";

        try (Connection conn = getConnection();
             PreparedStatement ps = conn.prepareStatement(saveRoomStatement)) {

            ps.setInt(1, roomDTO.getRoomNumber());
            ps.setInt(2, roomDTO.getSuiteDTO().getId());
            ps.setString(3, roomDTO.getDescription());
            ps.execute();
        } catch (SQLException e) {
            throw new RuntimeException("Could not add Room.");
        }
    }

    public static void update(RoomDTO room) {
        String updateSQL = "UPDATE " + ROOM_TABLE +
                " SET " + ROOM_NUMBER_COLUMN + " =? , " +
                SUITE_ID_COLUMN + " =? , " +
                DESCRIPTION_COLUMN + " =? " +
                " WHERE " + ID_COLUMN + " = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(updateSQL)) {

            ps.setInt(1, room.getRoomNumber());
            ps.setInt(2, room.getSuiteDTO().getId());
            ps.setString(3, room.getDescription());
            ps.setInt(4, room.getId());
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not update Suite.");
        }
    }

    public static void delete(int id) {
        String deleteSQL = "DELETE FROM " + ROOM_TABLE + " WHERE ID = ?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(deleteSQL)) {

            ps.setInt(1, id);
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Could not delete Room with id: " + id);
        }
    }

    public static Optional<RoomDTO> findRoomById(int id) {
        String selectSQL = "SELECT * FROM " + ROOM_TABLE + " AS RT" +
                " JOIN " + SUITE_TABLE + " AS ST ON RT." + SUITE_ID_COLUMN + " = ST." + ID_COLUMN +
                " WHERE RT." + ID_COLUMN + "=?;";

        try (Connection con = getConnection();
             PreparedStatement ps = con.prepareStatement(selectSQL)) {

            ps.setInt(1, id);
            ps.execute();

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(mapRoomDTO(rs));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not find Client by id: " + id);
        }
        return Optional.empty();
    }

    public static List<RoomDTO> findAll() {
        String findAll = "SELECT * " +
                " FROM " + ROOM_TABLE +
                " JOIN " + SUITE_TABLE + " ON " +
                ROOM_TABLE + '.' + SUITE_ID_COLUMN + '=' +
                SUITE_TABLE + '.' + ID_COLUMN +
                " ORDER BY " + ROOM_NUMBER_COLUMN + ';';

        return UtilDAO.findAll(findAll, RoomDAO::mapRoomDTO);
    }

    private static RoomDTO mapRoomDTO(ResultSet rs) {
        try {
            SuiteDTO suite =
                    new SuiteDTO(
                            SuiteType.valueOf(rs.getString(TYPE_COLUMN)),
                            rs.getInt(SIZE_COLUMN),
                            rs.getInt(PRICE_PER_NIGHT_COLUMN)
                    );

            return new RoomDTO(
                    rs.getInt(ID_COLUMN),
                    rs.getInt(ROOM_NUMBER_COLUMN),
                    suite,
                    rs.getString(DESCRIPTION_COLUMN)
            );
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
