package wantsome.project.business;

import wantsome.project.data.dao.ReservationDAO;

import java.sql.Date;

public class ReservationService {

    public static int getTotalPriceByIdReservation(int idReservation) {
        return ReservationDAO.getPriceToPay(idReservation);
    }

    public static int getDaysSpentAtHotel(Date startDate, Date endDate) {
        int numberOfDaysSpent = (int) ((Math.abs(endDate.getTime() - startDate.getTime()) / 86400000) + 1);
        return numberOfDaysSpent;
    }
}
