package wantsome.project.business;

import wantsome.project.data.dao.*;
import wantsome.project.data.dto.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.util.concurrent.TimeUnit;

import static wantsome.project.data.dao.AddressDAO.CREATE_ADDRESS_TABLE;
import static wantsome.project.data.dao.ClientDAO.CREATE_CLIENT_TABLE;
import static wantsome.project.data.dao.ReservationDAO.CREATE_RESERVATION_TABLE;
import static wantsome.project.data.dao.RoomDAO.CREATE_ROOM_TABLE;
import static wantsome.project.data.dao.SuiteDAO.CREATE_SUITE_TABLE;

public class DataService {

    public static void populateInitialData() {
        createTables();
        insertHotelRooms();
        insertDefaultData();
    }

    private static void createTables() {
        try (Connection conn = UtilDAO.getConnection();
             Statement st = conn.createStatement()) {
            st.execute(CREATE_SUITE_TABLE);
            st.execute(CREATE_ROOM_TABLE);
            st.execute(CREATE_ADDRESS_TABLE);
            st.execute(CREATE_CLIENT_TABLE);
            st.execute(CREATE_RESERVATION_TABLE);
        } catch (SQLException e) {
            throw new RuntimeException("Could not create tables.");
        }
    }

    private static void insertHotelRooms() {
        insertSuiteData();
        insertRoomData();
    }

    private static void insertSuiteData() {
        if (SuiteDAO.findAll().isEmpty()) {
            SuiteDAO.insert(new SuiteDTO(1, SuiteType.SINGLE, 1, 70));
            SuiteDAO.insert(new SuiteDTO(2, SuiteType.DOUBLE, 2, 100));
            SuiteDAO.insert(new SuiteDTO(3, SuiteType.TRIPLE, 3, 130));
            SuiteDAO.insert(new SuiteDTO(4, SuiteType.PENTHOUSE, 4, 150));
        }
    }

    private static void insertRoomData() {
        if (RoomDAO.findAll().isEmpty()) {
            RoomDAO.insert(new RoomDTO(101, new SuiteDTO(1), "Room 101"));
            RoomDAO.insert(new RoomDTO(102, new SuiteDTO(1), "Room 102"));
            RoomDAO.insert(new RoomDTO(103, new SuiteDTO(1), "Room 103"));
            RoomDAO.insert(new RoomDTO(104, new SuiteDTO(1), "Room 104"));
            RoomDAO.insert(new RoomDTO(201, new SuiteDTO(2), "Room 201"));
            RoomDAO.insert(new RoomDTO(202, new SuiteDTO(2), "Room 202"));
            RoomDAO.insert(new RoomDTO(203, new SuiteDTO(2), "Room 203"));
            RoomDAO.insert(new RoomDTO(204, new SuiteDTO(2), "Room 204"));
            RoomDAO.insert(new RoomDTO(205, new SuiteDTO(2), "Room 205"));
            RoomDAO.insert(new RoomDTO(206, new SuiteDTO(2), "Room 206"));
            RoomDAO.insert(new RoomDTO(207, new SuiteDTO(2), "Room 207"));
            RoomDAO.insert(new RoomDTO(208, new SuiteDTO(2), "Room 208"));
            RoomDAO.insert(new RoomDTO(209, new SuiteDTO(2), "Room 209"));
            RoomDAO.insert(new RoomDTO(210, new SuiteDTO(2), "Room 210"));
            RoomDAO.insert(new RoomDTO(301, new SuiteDTO(3), "Room 301"));
            RoomDAO.insert(new RoomDTO(302, new SuiteDTO(3), "Room 302"));
            RoomDAO.insert(new RoomDTO(303, new SuiteDTO(3), "Room 303"));
            RoomDAO.insert(new RoomDTO(304, new SuiteDTO(3), "Room 304"));
            RoomDAO.insert(new RoomDTO(305, new SuiteDTO(3), "Room 305"));
            RoomDAO.insert(new RoomDTO(306, new SuiteDTO(3), "Room 306"));
            RoomDAO.insert(new RoomDTO(307, new SuiteDTO(3), "Room 307"));
            RoomDAO.insert(new RoomDTO(308, new SuiteDTO(3), "Room 308"));
            RoomDAO.insert(new RoomDTO(401, new SuiteDTO(4), "Room 401"));
            RoomDAO.insert(new RoomDTO(402, new SuiteDTO(4), "Room 402"));
            RoomDAO.insert(new RoomDTO(403, new SuiteDTO(4), "Room 403"));
            RoomDAO.insert(new RoomDTO(404, new SuiteDTO(4), "Room 404"));
        }
    }

    private static void insertDefaultData() {
        insertDefaultAddressData();
        insertDefaultClientData();
        insertDefaultReservationData();
    }

    private static void insertDefaultAddressData() {
        if (AddressDAO.findAll().isEmpty()) {
            AddressDAO.insert(
                    new AddressDTO("Alee Copou nr. 1-T", "700460", "Iasi", "Romania")
            );
            AddressDAO.insert(
                    new AddressDTO("Bld. Tudor Vladimirescu nr. 23", "700230", "Iasi", "Romania")
            );
            AddressDAO.insert(
                    new AddressDTO("Podu Ros nr. 45", "700310", "Iasi", "Romania")
            );
        }
    }

    private static void insertDefaultClientData() {
        if (ClientDAO.findAll().isEmpty()) {
            ClientDAO.insert(
                    new ClientDTO("Alin", "Amarioarei", 1111111, "alin@gmail.com", 1)
            );
            ClientDAO.insert(
                    new ClientDTO("George", "Balan", 2222222, "george@gmail.com", 2)
            );
            ClientDAO.insert(
                    new ClientDTO("Sorin", "Scutaru", 3333333, "sorin@gmail.com",
                            AddressDAO.getLastIdInserted())
            );
        }
    }

    private static void insertDefaultReservationData() {
        if (ReservationDAO.findAll().isEmpty()) {
            ReservationDAO.insert(
                    new ReservationDTO(1, 21, new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(5)),
                            new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(8)))
            );
            ReservationDAO.insert(
                    new ReservationDTO(2, 13, new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(10)),
                            new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(13)))
            );
            ReservationDAO.insert(
                    new ReservationDTO(3, 24, new Date(System.currentTimeMillis()),
                            new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(3)))
            );
        }
    }
}
