package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.AddressDAO;
import wantsome.project.data.dao.ClientDAO;
import wantsome.project.data.dao.ReservationDAO;
import wantsome.project.data.dto.AddressDTO;
import wantsome.project.data.dto.ClientDTO;
import wantsome.project.data.dto.ReservationDTO;
import wantsome.project.data.dto.RoomDTO;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static wantsome.project.data.dao.UtilDAO.getCurrency;
import static wantsome.project.web.SparkUtil.render;

public class AddEditReservationsController {

    public static Object getAddReservationPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        return render("addNew_reservation.vm", model);
    }

    public static Object addNewReservation(Request request, Response response) {
        Date startDate = Date.valueOf(request.queryParams("startDate"));
        Date endDate = Date.valueOf(request.queryParams("endDate"));
        int roomId = Integer.parseInt(request.queryParams("roomId"));

        return renderAddNewReservation(request, response, roomId, startDate, endDate);
    }

    private static Object renderAddNewReservation(Request request, Response response, int roomId, Date startDate, Date endDate) {
        String fName = request.queryParams("firstName");
        String lName = request.queryParams("lastName");
        String cnpStr = request.queryParams("cnp");
        String email = request.queryParams("email");
        String street = request.queryParams("street");
        String zipcode = request.queryParams("zipcode");
        String city = request.queryParams("city");
        String country = request.queryParams("country");

        long cnp = Long.parseLong(cnpStr);

        AddressDTO newAddress = new AddressDTO(street, zipcode, city, country);
        AddressDAO.insert(newAddress);
        int idAddress = AddressDAO.getLastIdInserted();

        ClientDAO.insert(new ClientDTO(fName, lName, cnp, email, idAddress));
        int idClient = ClientDAO.getLastIdInserted();

        ReservationDAO.insert(new ReservationDTO(idClient, roomId, startDate, endDate));

        return render("message.vm", singletonMap("message",
                "A new Reservation was added successfully."));
    }

    public static Object getAvailableRoomsPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        return render("available_rooms.vm", model);
    }

    public static Object showAvailableRooms(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        String startDateStr = request.queryParams("startDate");
        String endDateStr = request.queryParams("endDate");
        Date startDate = Date.valueOf(startDateStr);
        Date endDate = Date.valueOf(endDateStr);

        model.put("start", startDate);
        model.put("end", endDate);

        List<RoomDTO> availableRooms = new ArrayList<>(ReservationDAO.getAvailableRoomsByDate(startDate, endDate));

        model.put("roomsInfo", availableRooms);
        model.put("currency", getCurrency());

        return render("available_rooms.vm", model);
    }

    public static Object getExistingClientAndBookRoom(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        return render("addSimple_reservation.vm", model);
    }

    public static Object renderReservationForExistingClient(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        Date startDate = Date.valueOf(request.queryParams("startDate"));
        Date endDate = Date.valueOf(request.queryParams("endDate"));
        int roomId = Integer.parseInt(request.queryParams("roomId"));
        int roomNumber = Integer.parseInt(request.queryParams("roomNumber"));

        model.put("startDate", startDate);
        model.put("endDate", endDate);
        model.put("roomId", roomId);
        model.put("roomNumber", roomNumber);

        String findFirstName = request.queryParams("findFirstName");
        String findLastName = request.queryParams("findLastName");
        long findCnp = Long.parseLong(request.queryParams("findCNP"));

        List<ClientDTO> existingClient = ClientDAO.getCustomerInfoByNameAndCNP(findFirstName, findLastName, findCnp);

        model.put("clientsInfo", existingClient);

        return render("addSimple_reservation.vm", model);
    }

    public static Object addReservationForExistingClient(Request request, Response response) {
        int clientId = Integer.parseInt(request.queryParams("clientId"));
        int roomId = Integer.parseInt(request.queryParams("roomId"));
        Date startDate = Date.valueOf(request.queryParams("startDate"));
        Date endDate = Date.valueOf(request.queryParams("endDate"));

        ReservationDAO.insert(new ReservationDTO(clientId, roomId, startDate, endDate));

        return render("message.vm", singletonMap("message",
                "A new Reservation was added succesfully for Client with id " + clientId + '.'));
    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String idReservationStr = request.params(":id");
        int idReservation = Integer.parseInt(idReservationStr);
        ReservationDAO.delete(idReservation);
        return render("message.vm", singletonMap("message",
                "The reservation with id " + idReservationStr + " was deleted!"));
    }
}
