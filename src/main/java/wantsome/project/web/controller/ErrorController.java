package wantsome.project.web.controller;

import spark.Request;
import spark.Response;

import static java.util.Collections.singletonMap;
import static wantsome.project.web.SparkUtil.render;

public class ErrorController {

    public static void handleException(Exception exception, Request request, Response response) {
        response.body(render("error.vm", singletonMap("errorMessage", exception.getMessage())));
    }
}
