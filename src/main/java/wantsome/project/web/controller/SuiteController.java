package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.SuiteDAO;
import wantsome.project.data.dto.SuiteDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;
import static wantsome.project.data.dao.UtilDAO.getCurrency;
import static wantsome.project.web.SparkUtil.render;

public class SuiteController {

    public static Object showAllSuites(Request request, Response response) {
        HashMap<String, Object> model = new HashMap<>();

        List<SuiteDTO> suitesInfo = SuiteDAO.findAll();
        model.put("suitesInfo", suitesInfo);
        model.put("currency", getCurrency());

        return render("suites.vm", model);
    }

    public static Object getSuiteUpdatePage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        model.put("type", request.queryParams("suiteType"));
        model.put("size", request.queryParams("suiteSize"));
        model.put("prevPrice", request.queryParams("suitePrice"));
        return render("edit_suite.vm", model);
    }

    public static Object renderSuiteUpdate(Request request, Response response) {
        int suiteId = Integer.parseInt(request.queryParams("suiteId"));
        int newPrice = Integer.parseInt(request.queryParams("newPrice"));

        SuiteDAO.update(suiteId, newPrice);

        return render("message.vm", singletonMap("message",
                "The Suite with id " + suiteId + " was updated."));
    }
}
