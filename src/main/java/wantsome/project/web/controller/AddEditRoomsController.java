package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.RoomDAO;
import wantsome.project.data.dao.SuiteDAO;
import wantsome.project.data.dto.RoomDTO;
import wantsome.project.data.dto.SuiteDTO;
import wantsome.project.data.dto.SuiteType;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.singletonMap;
import static wantsome.project.web.SparkUtil.render;

public class AddEditRoomsController {

    public static Object getAddRoomPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        model.put("suites", SuiteDAO.findAll());
        return render("add_room.vm", model);
    }

    public static Object renderAddRoomPage(Request request, Response response) {
        int roomNumber = Integer.parseInt(request.queryParams("roomNumber"));
        String description = request.queryParams("description");
        String suiteType = request.queryParams("suiteType");

        SuiteDTO suiteDTO = SuiteDAO.findByType(suiteType);
        RoomDAO.insert(new RoomDTO(roomNumber, suiteDTO, description));

        return render("message.vm", singletonMap("message",
                "A new Room was added successfully."));
    }

    public static Object showUpdate(Request request, Response response) {
        String idStr = request.queryParams("roomId");
        int id = Integer.parseInt(idStr);
        Optional<RoomDTO> optRoom = RoomDAO.findRoomById(id);
        RoomDTO room = optRoom.get();

        return renderUpdate(room.getRoomNumber(), room.getSuiteDTO().getType(), room.getDescription());
    }

    private static Object renderUpdate(int roomNumber, SuiteType type, String description) {
        Map<String, Object> model = new HashMap<>();

        model.put("prevRoomNumber", roomNumber);
        model.put("suites", SuiteDAO.findAll());
        model.put("prevType", type);
        model.put("prevDescription", description);

        return render("edit_room.vm", model);
    }

    public static Object handleUpdateRequest(Request request, Response response) {
        int id = Integer.parseInt(request.queryParams("roomId"));
        int roomNumber = Integer.parseInt(request.queryParams("roomNumber"));
        String description = request.queryParams("description");
        String suiteTypeStr = request.queryParams("suiteType");
        SuiteDTO suite = SuiteDAO.findByType(suiteTypeStr);

        return tryToUpdate(response, id, roomNumber, description, suite);
    }

    private static Object tryToUpdate(Response response, int id, int roomNumber, String description, SuiteDTO suite) {
        RoomDTO roomToUpdate = new RoomDTO(id, roomNumber, suite, description);
        RoomDAO.update(roomToUpdate);

        return render("message.vm", singletonMap("message",
                "The Room with id " + id + " was updated."));
    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String idStr = request.params(":id");
        int roomId = Integer.parseInt(idStr);
        RoomDAO.delete(roomId);
        return render("message.vm", singletonMap("message",
                "The Room with id " + roomId + " was deleted!"));
    }
}
