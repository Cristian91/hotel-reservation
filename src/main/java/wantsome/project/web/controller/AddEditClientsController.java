package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.AddressDAO;
import wantsome.project.data.dao.ClientDAO;
import wantsome.project.data.dto.AddressDTO;
import wantsome.project.data.dto.ClientDTO;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.singletonMap;
import static wantsome.project.web.SparkUtil.render;
import static wantsome.project.web.controller.ClientsController.getClientsWebPage;

public class AddEditClientsController {

    public static Object getAddClientPage(Request request, Response response) {
        HashMap<String, Object> model = new HashMap<>();
        return render("add_client.vm", model);
    }

    public static Object addNewClient(Request request, Response response) {
        String fName = request.queryParams("firstName");
        String lName = request.queryParams("lastName");
        String cnpStr = request.queryParams("cnp");
        String email = request.queryParams("email");
        String street = request.queryParams("street");
        String zipcode = request.queryParams("zipcode");
        String city = request.queryParams("city");
        String country = request.queryParams("country");

        long cnp = Long.parseLong(cnpStr);

        AddressDTO newAddress = new AddressDTO(street, zipcode, city, country);
        AddressDAO.insert(newAddress);
        int idAddress = AddressDAO.getLastIdInserted();
        ClientDAO.insert(new ClientDTO(fName, lName, cnp, email, idAddress));

        return render("message.vm", singletonMap("message",
                "A new Client was added successfully."));
    }

    public static Object showUpdate(Request request, Response response) {
        String idStr = request.queryParams("clientId");
        int id = Integer.parseInt(idStr);
        Optional<ClientDTO> optClient = ClientDAO.findClientById(id);
        ClientDTO client = optClient.get();

        return renderUpdate(
                client.getFirstName(),
                client.getLastName(),
                client.getCnp(),
                client.getEmail(),
                client.getAddress().getStreetNumber(),
                client.getAddress().getZipCode(),
                client.getAddress().getCity(),
                client.getAddress().getCountry());
    }

    private static Object renderUpdate(String firstName, String lastName, long cnp, String email, String streetNumber, String zipCode, String city, String country) {
        Map<String, Object> model = new HashMap<>();

        model.put("prevFirstName", firstName);
        model.put("prevLastName", lastName);
        model.put("prevCNP", cnp);
        model.put("prevEmail", email);
        model.put("prevStreet", streetNumber);
        model.put("prevZipCode", zipCode);
        model.put("prevCity", city);
        model.put("prevCountry", country);

        return render("edit_client.vm", model);
    }

    public static Object handleUpdateRequest(Request request, Response response) {
        String idStr = request.queryParams("clientId");
        String fName = request.queryParams("firstName");
        String lName = request.queryParams("lastName");
        String cnpStr = request.queryParams("cnp");
        String email = request.queryParams("email");
        String street = request.queryParams("street");
        String zipcode = request.queryParams("zipcode");
        String city = request.queryParams("city");
        String country = request.queryParams("country");

        return tryToUpdate(
                response, idStr, fName, lName, cnpStr, email, street, zipcode, city, country
        );
    }

    private static Object tryToUpdate(Response response, String idStr, String fName, String lName, String cnpStr, String email, String street, String zipcode, String city, String country) {
        int idClient = Integer.parseInt(idStr);
        long cnp = Long.parseLong(cnpStr);
        int addressId = ClientDAO.findAddressIdByClientId(idClient);

        AddressDTO address = new AddressDTO(addressId, street, zipcode, city, country);
        AddressDAO.update(address);

        ClientDTO client = new ClientDTO(idClient, fName, lName, cnp, email, address);
        ClientDAO.update(client);

        return render("message.vm", singletonMap("message",
                "The Client with id " + idClient + " was updated."));
    }

    public static Object handleDeleteRequest(Request request, Response response) {
        String idStr = request.params(":id");
        int idClient = Integer.parseInt(idStr);
        int idAddress = ClientDAO.findAddressIdByClientId(idClient);
        ClientDAO.delete(idClient);
        AddressDAO.delete(idAddress);
        return render("message.vm", singletonMap("message",
                "The Client with id " + idClient + " was deleted!"));
    }
}
