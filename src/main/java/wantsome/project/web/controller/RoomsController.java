package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.RoomDAO;
import wantsome.project.data.dto.RoomDTO;

import java.util.HashMap;
import java.util.List;

import static wantsome.project.data.dao.UtilDAO.getCurrency;
import static wantsome.project.web.SparkUtil.render;

public class RoomsController {

    public static Object getRoomsWebPage(Request request, Response response) {
        HashMap<String, Object> model = new HashMap<>();

        List<RoomDTO> roomsInfo = RoomDAO.findAll();
        model.put("roomsInfo", roomsInfo);
        model.put("currency", getCurrency());

        return render("rooms.vm", model);
    }
}
