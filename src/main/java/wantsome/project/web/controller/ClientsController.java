package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.data.dao.ClientDAO;
import wantsome.project.data.dto.ClientDTO;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class ClientsController {

    public static Object getClientsWebPage(Request request, Response response) {
        HashMap<String, Object> model = new HashMap<>();
        model.put("serverTime", new Date(System.currentTimeMillis()));

        List<ClientDTO> clientsInfo = ClientDAO.findAll();
        model.put("clientsInfo", clientsInfo);

        return render("clients.vm", model);
    }

    public static Object findClient(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        return render("find_client.vm", model);
    }

    public static Object renderFindClient(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        String firstName = request.queryParams("firstName");
        String lastName = request.queryParams("lastName");
        long cnp = Long.parseLong(request.queryParams("cnp"));

        List<ClientDTO> client = ClientDAO.getCustomerInfoByNameAndCNP(firstName, lastName, cnp);

        model.put("clientInfo", client);

        return render("find_client.vm", model);
    }
}
