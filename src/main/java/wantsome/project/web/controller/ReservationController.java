package wantsome.project.web.controller;

import spark.Request;
import spark.Response;
import wantsome.project.business.ReservationService;
import wantsome.project.data.dao.ReservationDAO;
import wantsome.project.data.dao.RoomDAO;
import wantsome.project.data.dto.ReservationDTO;
import wantsome.project.data.dto.RoomDTO;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collector;

import static wantsome.project.data.dao.UtilDAO.getCurrency;
import static wantsome.project.web.SparkUtil.render;

public class ReservationController {

    public static Object getReservationsWebPage(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();

        List<ReservationDTO> reservations = ReservationDAO.findAll();
        model.put("reservations", reservations);

        return render("reservations.vm", model);
    }

    public static Object getPaymentPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        int reservationId = Integer.parseInt(request.queryParams("resId"));
        int priceToPay = ReservationService.getTotalPriceByIdReservation(reservationId);
        ReservationDTO reservation = ReservationDAO.getReservationById(reservationId);
        model.put("resId", reservation.getId());
        model.put("firstName", reservation.getClient().getFirstName());
        model.put("lastName", reservation.getClient().getLastName());
        model.put("cnp", reservation.getClient().getCnp());
        model.put("roomNumber", reservation.getRoom().getRoomNumber());
        model.put("roomPrice", reservation.getRoom().getSuiteDTO().getPrice());
        model.put("startDate", reservation.getStartDate());
        model.put("endDate", reservation.getEndDate());
        model.put("daysSpent", ReservationService.getDaysSpentAtHotel(reservation.getStartDate(), reservation.getEndDate()));
        model.put("totalPrice", priceToPay);
        model.put("currency", getCurrency());
        return render("payment_reservation.vm", model);
    }

    public static Object findReservationByClientPage(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        return render("find_reservation.vm", model);
    }

    public static Object renderReservationByClient(Request request, Response response) {
        Map<String, Object> model = new HashMap<>();
        String firstName = request.queryParams("firstName");
        String lastName = request.queryParams("lastName");
        long cnp = Long.parseLong(request.queryParams("cnp"));

        List<ReservationDTO> reservations = ReservationDAO.getReservationByClient(firstName, lastName, cnp);

        model.put("reservations", reservations);

        return render("find_reservation.vm", model);
    }
}
