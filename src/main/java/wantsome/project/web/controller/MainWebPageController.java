package wantsome.project.web.controller;

import spark.Request;
import spark.Response;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import static wantsome.project.web.SparkUtil.render;

public class MainWebPageController {

    public static Object getMainWebPage(Request req, Response res) {
        Map<String, Object> model = new HashMap<>();
        model.put("serverTime", new Date(System.currentTimeMillis()).toString());

        return render("main_web.vm", model);
    }
}
