package wantsome.project;

import wantsome.project.business.DataService;
import wantsome.project.web.controller.*;

import static spark.Spark.*;


/**
 * Main class of the application. Using Spark framework.
 */
public class Main {

    public static void main(String[] args) {
        setup();
        configureRoutesAndStart();
    }

    private static void setup() {
        DataService.populateInitialData();
    }

    private static void configureRoutesAndStart() {
        staticFileLocation("public");

        //-----MAIN-----//
        get("/main", MainWebPageController::getMainWebPage);

        //-----RESERVATIONS-----//
        get("/reservations", ReservationController::getReservationsWebPage);

        get("/reservations/find", ReservationController::findReservationByClientPage);
        post("/reservations/find", ReservationController::renderReservationByClient);

        get("/reservations/addNew", AddEditReservationsController::getAddReservationPage);
        post("/reservations/addNew", AddEditReservationsController::addNewReservation);

        get("/reservations/addSimple", AddEditReservationsController::getExistingClientAndBookRoom);
        post("/reservations/addSimple", AddEditReservationsController::renderReservationForExistingClient);

        get("/reservations/addSimpleReservation", AddEditReservationsController::addReservationForExistingClient);

        get("/reservations/available", AddEditReservationsController::getAvailableRoomsPage);
        post("/reservations/available", AddEditReservationsController::showAvailableRooms);

        get("/reservations/payment", ReservationController::getPaymentPage);

        get("/reservations/delete/:id", AddEditReservationsController::handleDeleteRequest);

        //-----CLIENTS-----//
        get("/clients", ClientsController::getClientsWebPage);

        get("/clients/find", ClientsController::findClient);
        post("/clients/find", ClientsController::renderFindClient);

        get("/clients/add", AddEditClientsController::getAddClientPage);
        post("/clients/add", AddEditClientsController::addNewClient);

        get("/clients/update", AddEditClientsController::showUpdate);
        post("/clients/update", AddEditClientsController::handleUpdateRequest);

        get("/clients/delete/:id", AddEditClientsController::handleDeleteRequest);

        //-----ROOMS-----//
        get("/rooms", RoomsController::getRoomsWebPage);

        get("/rooms/add", AddEditRoomsController::getAddRoomPage);
        post("/rooms/add", AddEditRoomsController::renderAddRoomPage);

        get("/rooms/update", AddEditRoomsController::showUpdate);
        post("/rooms/update", AddEditRoomsController::handleUpdateRequest);

        get("/rooms/delete/:id", AddEditRoomsController::handleDeleteRequest);

        //-----SUITE-----//
        get("/suites", SuiteController::showAllSuites);

        get("/suites/update", SuiteController::getSuiteUpdatePage);
        post("/suites/update", SuiteController::renderSuiteUpdate);

        //-----ErrorHandling-----//
        exception(Exception.class, ErrorController::handleException);

        awaitInitialization();
        System.out.println("\nServer started: http://localhost:4567/main");
    }
}
